/* 
 * Format's of list
 * Key and Format name should be same
 * Display Format is the name which we have to display(with spaces)
 */
export const Format = Object.freeze({
    Novel: {
        Format: "Novel",
        groupPrefix: "Chapter",
        cardPrefix: "Scene",
        groupUIName: "Chapter",
        cardUIName: "Scene"
    },
    Comic: {
        Format: "Comic",
        groupPrefix: "Page",
        cardPrefix: "Panel",
        groupUIName: "Page",
        cardUIName: "Panel"

    },
    Essay: {
        Format: "Essay",
        groupPrefix: "",
        cardPrefix: "",
        groupUIName: "Section",
        cardUIName: "Sub-Section"

    }, 
    Screenplay: {
        Format: "Screenplay",
        groupPrefix: "Act",
        cardPrefix: "Scene",
        groupUIName: "Act",
        cardUIName: "Scene"

        },
    Blank: {
        Format: "Blank",
        groupPrefix: "",
        cardPrefix: "",
        groupUIName: "Group",
        cardUIName: ""
    },
})
