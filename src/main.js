import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import axios from 'axios'

import { BootstrapVue, BootstrapVueIcons } from 'bootstrap-vue'
import VueResource from "vue-resource"
import './assets/custom.scss'

Vue.use(require('vue-moment'));  
Vue.use(BootstrapVue)
Vue.use(BootstrapVueIcons)
Vue.use(VueResource);

Vue.http.options.emulateJSON = true;
axios.defaults.baseURL=process.env.VUE_APP_BASE_API_URL
axios.interceptors.response.use(
  response => response,
  error => {
      if (error.response.status == 401 && router.currentRoute.name != 'login') {
        router.push({ name: 'login' })
      }
      return Promise.reject(error)
  }
)

Vue.config.productionTip = false
export const bus = new Vue(); 

new Vue({
  router,
  store,
  created () {
    let user = localStorage.getItem('user')
    if (user) {
      this.$store.commit('auth/SET_USER', JSON.parse(user))
      axios.get('me')
    }
  },
  render: h => h(App)
}).$mount('#app');
